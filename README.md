# ssp-sdn-ddos

Project is about to demonstrate *Slowloris DDoS attack* and its mitigation using *Snort* engine.

## Getting started

To demonstrate how *Slowloris* attack can be mitigated by *Snort* engine we're going to use following topology:

![topology](static/topology.svg 'topology')

In this scenario *attacker (h1)*, is performing attack on the network segment: *server, h2, h3, h4* are potential victims attacker is targeting. Switch *s1* is controlled via Ryu *controller*.

*Snort* can be easily integrated with *Ryu* controller via *UNIX sockets*. More info can be found in the [official documentation](https://ryu.readthedocs.io/en/latest/snort_integrate.html).

*Mininet* implementation of this topology can be found in: `mininet/topology.py`.

### Slowloris

We'll be using already prepared python application that is publicly opened under: https://github.com/gkbrk/slowloris.
**NOTE:** Before proceeding with *Slowloris*, python needs to be updated to ver. at least 3.7, this can be done via following on *Mininet* VM (*Mininet* official VM comes with *python3.6*, at least at the day of this writing):

```sh
apt-get update
apt-get upgrade -y
add-apt-repository ppa:ppaname/ppa
apt-get install --reinstall ca-certificates
apt-get update 
apt-get install python3.7 -y
```

Additionally we want to install *Slowloris* script:

```sh
git clone https://github.com/gkbrk/slowloris.git
```

After this, the script can be run easily on each node via:

```sh
cd slowloris
python3.7 slowloris <TARGET_IP>
```

### Simple scenario

In this scenario we're providing commands, that when executed, can perfrom *Slowloris* attack and stop it via *Snort* altert directed to the controller.

1. First of all we want to run our controller with *Mininet* topology:

```
sudo ryu-manager simple_switch_snort.py
sudo mn --custom topology.py --topo slowLoris --controller=remote,ip=127.0.0.1,port=6653
```

2. Then we can test connectivity between e.g h1 <-> h2 by executing `h1 ping h2` in *mn* cli.

3. After we've successful response from ping, we can deploy simple HTTP server on `h2` executing: `xterm h2` and then: `python3 -m http.server 80`.

4. Starting *Snort* can be done via: `sudo snort -i s1-eth1 -A unsock -l /tmp -c /etc/snort/snort.conf -k none`, **NOTE**: `-k none` is crucial here.

5. *Slowloris* attack from *h1* to *h2* can be performed via: `sudo slowloris -p 80 10.0.0.2`.

6. You can check controller flows via: `sudo ovs-ofctl dump-flows s1`.

### Presentation

Presentation used during the classes can be found [here](./static/ssp_slowloris.pptx.pdf)
