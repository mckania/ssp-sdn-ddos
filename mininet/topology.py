from mininet.topo import Topo
from mininet.log import info

'''
This is implementation of the topology used as a demo.
To run this custom topology: `sudo mn --custom topology.py --topo slowLoris`
'''

class SlowLorisTopo(Topo):

  def __init__(self):
    Topo.__init__(self)

    info('*** Adding hosts...***\n')
    h1 = self.addHost('h1')
    h2 = self.addHost('h2')
    h3 = self.addHost('h3')
    h4 = self.addHost('h4')

    info('*** Adding server and switch***\n')
    server = self.addHost('server')
    s1 = self.addSwitch('s1')

    info('*** Adding links...***\n')
    self.addLink(h1, s1)
    self.addLink(h2, s1)
    self.addLink(h3, s1)
    self.addLink(h4, s1)
    self.addLink(server, s1)

topos = {'slowLoris': (lambda: SlowLorisTopo())}
